#!/usr/bin/env python
import io
import os
import sys

#Set UTF-8 encoding to display results in terminal
reload(sys)
sys.setdefaultencoding('utf-8')

# Imports the Google Cloud client library
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

def test_jp():
	# Instantiates a client
	client = speech.SpeechClient()

	# The name of the audio file to transcribe
	file_name = os.path.join(
		os.path.dirname(__file__),
		'resources',
		'nhk-final.flac')

	# Loads the audio into memory
	with io.open(file_name, 'rb') as audio_file:
		content = audio_file.read()
		audio = types.RecognitionAudio(content=content)

	config = types.RecognitionConfig(
		encoding = enums.RecognitionConfig.AudioEncoding.FLAC,
		sample_rate_hertz = 32000,
		language_code = 'ja-JP')

	# Detects speech in the audio file
	response = client.recognize(config, audio)

	for result in response.results:
		print('Transcript: {}'.format(result.alternatives[0].transcript))

if __name__ == '__main__':
	test_jp()
